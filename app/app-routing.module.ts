import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
    // { path: "", loadChildren: "./task/task.module#TaskModule" },
    // { path: "", loadChildren: "./instruction/instruction.module#InstructionModule" },    
    // { path: "", loadChildren: "./history/history.module#HistoryModule" },        
    { path: "", loadChildren: "./login/login.module#LoginModule" },    
    { path: "instruction", loadChildren: "./instruction/instruction.module#InstructionModule" },    
    { path: "home", loadChildren: "./home/home.module#HomeModule" },
    { path: "task", loadChildren: "./task/task.module#TaskModule" },
    { path: "profile", loadChildren: "./profile/profile.module#ProfileModule" },
    { path: "statTransfer", loadChildren: "./statTransfer/statTransfer.module#StatTransferModule" }    
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
