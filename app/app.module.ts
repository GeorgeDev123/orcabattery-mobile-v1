import { NgModule, NgModuleFactoryLoader, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NSModuleFactoryLoader } from "nativescript-angular/router";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpModule } from "nativescript-angular";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { SettingService, UserService, TaskService } from "./shared";
import { LoginModule } from "./login/login.module";
import { HomeModule } from "./home/home.module";
import { DrawerComponent } from "./sub-component/drawer/drawer.component";
import { NativeScriptUISideDrawerModule } from "nativescript-telerik-ui-pro/sidedrawer/angular";
import { SubComponentModule } from "./sub-component/sub-component.module";
import { TaskModule } from "./task/task.module";
import { InstructionModule } from "./instruction/instruction.module";
import { ProfileModule } from "./profile/profile.module";
import { StatTransferModule } from "./statTransfer/statTransfer.module";
import { HistoryModule } from "./history/history.module";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptFormsModule,
        NativeScriptHttpModule,
        LoginModule,
        HomeModule,
        TaskModule,
        InstructionModule,
        NativeScriptUISideDrawerModule,
        SubComponentModule,
        ProfileModule,
        StatTransferModule,
        HistoryModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        { provide: NgModuleFactoryLoader, useClass: NSModuleFactoryLoader },
        SettingService,
        UserService,
        TaskService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
