import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from "@angular/core";
import { DrawerTransitionBase, SlideInOnTopTransition } from "nativescript-telerik-ui-pro/sidedrawer";
import { RadSideDrawerComponent } from "nativescript-telerik-ui-pro/sidedrawer/angular";
import { UserService, User, SettingService, TaskService, Session, Task, TaskStatus, Block, TaskVM } from "../shared";
import { Router } from "@angular/router";
import * as dialogs from "ui/dialogs";
import { RouterExtensions } from "nativescript-angular";
import * as Rx from "rxjs/Rx";
import { Observable } from "rxjs/Rx";
import { Subscription } from "rxjs/Subscription";
import { TimerObservable } from "rxjs/observable/TimerObservable";
import * as moment from "moment";
import * as LocalNotifications from "nativescript-local-notifications";


@Component({
    selector: "History",
    moduleId: module.id,
    templateUrl: "./history.component.html"
})

export class HistoryComponent implements OnInit {
    public input: any;
    
    @ViewChild("drawer") drawerComponent: RadSideDrawerComponent;
    private _sideDrawerTransition: DrawerTransitionBase;
    constructor(private userService: UserService, private router: Router, private taskService: TaskService) {
        this.input = {
            "id": "12345",
            "title": "",
            "body": ""
        };
    }

    ngOnInit(): void {
        this._sideDrawerTransition = new SlideInOnTopTransition();
        this.drawerComponent.sideDrawer.closeDrawer();
        LocalNotifications.addOnMessageReceivedCallback(notificationData => {
            dialogs.alert({
                title: "Notification received",
                message: "ID: " + notificationData.id +
                "\nTitle: " + notificationData.title +
                "\nBody: " + notificationData.body,
                okButtonText: "Excellent!"
            });
        }
    );

    }
    onDrawerButtonTap(): void {
        this.drawerComponent.sideDrawer.showDrawer();
    }
    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }
    public schedule() {
        LocalNotifications.requestPermission().then(granted => {
            debugger
            if(granted) {
                LocalNotifications.schedule([{
                    id: this.input.id,
                    title: this.input.title,
                    body: this.input.body,
                    at: new Date(new Date().getTime() + (10 * 1000))
                }]).then(() => {
                    alert("Notification showed");
                }, error => {
                    console.dir(error);
                });
            }
        });
    }

}
