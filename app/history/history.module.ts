import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { HistoryRoutingModule } from "./history-routing.module";
import { HistoryComponent } from "./history.component";
import { NativeScriptUISideDrawerModule } from "nativescript-telerik-ui-pro/sidedrawer/angular";
import { DrawerComponent } from "../sub-component/drawer/drawer.component";
import { SubComponentModule } from "../sub-component/sub-component.module";

@NgModule({
    imports: [
        NativeScriptModule,
        HistoryRoutingModule,
        NativeScriptUISideDrawerModule,
        SubComponentModule
    ],
    declarations: [
        HistoryComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class HistoryModule { }
