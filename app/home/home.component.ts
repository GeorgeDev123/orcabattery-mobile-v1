import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from "@angular/core";
import { DrawerTransitionBase, SlideInOnTopTransition } from "nativescript-telerik-ui-pro/sidedrawer";
import { RadSideDrawerComponent } from "nativescript-telerik-ui-pro/sidedrawer/angular";
import { UserService, User, SettingService, TaskService, Session, Task, TaskStatus, Block, TaskVM } from "../shared";
import { Router } from "@angular/router";
import * as dialogs from "ui/dialogs";
import { RouterExtensions } from "nativescript-angular";
import * as Rx from "rxjs/Rx";
import { Observable } from "rxjs/Rx";
import { Subscription } from "rxjs/Subscription";
import { TimerObservable } from "rxjs/observable/TimerObservable";
import * as moment from "moment";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html"
})

export class HomeComponent implements OnInit, AfterViewInit {
    ngAfterViewInit(): void {
        dialogs.alert({
            title: "Login",
            message: this.getPopUpText(),
            okButtonText: "Ok"
        }).then(() => {
            console.log("Done alert");
        });
    }

    @ViewChild("drawer") drawerComponent: RadSideDrawerComponent;
    user: User;
    txtAlert: string;
    public tasksVM = new Array<TaskVM>();
    tasks: Task[];
    currentBlockIndex: number;
    currentBlock: Block;
    task1Name: string;
    countDown: string;
    private _sideDrawerTransition: DrawerTransitionBase;
    constructor(private userService: UserService, private router: Router, private taskService: TaskService) {
        this.user = JSON.parse(SettingService.participantDetails);
    }

    ngOnInit(): void {
        let tasksUrl = JSON.parse(SettingService.tasksUrl);
        this.currentBlockIndex = -1;
        this.userService.tasks(tasksUrl)
            .subscribe(results => {
                SettingService.tasks = JSON.stringify(results);
                let tasks = JSON.parse(SettingService.tasks);
                this.tasks = this.taskService.createTasks(tasks);
                this.tasksVM = this.tasks.map(task => {
                    this.setOrUpdateCurrentTaskDetails(task);
                    return this.setTaskVM(task);
                });
                this.user.tasks = this.tasks;
                SettingService.participantDetails = JSON.stringify(this.user);
            });

        this._sideDrawerTransition = new SlideInOnTopTransition();
        // dialogs.alert({
        //     title: "Welcome",
        //     message: this.getPopUpText(),
        //     okButtonText: "Ok"
        // }).then(() => {
        //     console.log("Done alert");
        // });
    }

    private setTaskVM(task: Task): TaskVM {
        let newTaskVm = new TaskVM({
            name: task.taskSpecification.name,
            status: this.getTaskStatus(task),
            completionPercentage: task.completionPercentage
        });
        newTaskVm.isCurrentBolck = task.status === TaskStatus.STARTED ? true : false;
        return newTaskVm;
    }

    private setOrUpdateCurrentTaskDetails(task: Task): void {
        if (task.status !== TaskStatus.STARTED) {
            return
        }
        this.taskService.currentTask = task;
        if (this.taskService.currentTask.currentSessionHref) {
            this.userService.commonGetHttpCall(this.taskService.currentTask.currentSessionHref)
                .subscribe(session => {
                    console.log("[Succssfull get details of a session: ]" + this.taskService.currentTask.sessionHref);
                    this.taskService.setSession(session);
                    if (this.taskService.currentTask.transferHref && this.taskService.currentTask.currentSession.status === TaskStatus.COMPLETED) {
                        this.userService.commonGetHttpCall(this.taskService.currentTask.transferHref).subscribe(transfer => {
                            this.taskService.setTransfer(transfer);
                            this.currentBlock = this.taskService.getCurrentAcitveBlock();
                        })
                    }
                    else if (this.taskService.currentTask.currentSession.currentBlockHref) {
                        this.userService.commonGetHttpCall(this.taskService.currentTask.currentSession.currentBlockHref)
                            .subscribe(block => {
                                console.log("[Succssfull get details of a block: ]" + this.taskService.currentTask.currentSession.currentBlockHref);
                                this.taskService.setBlock(block);
                                this.currentBlock = this.taskService.getCurrentAcitveBlock();
                            }),
                            (error: any) => {
                                console.log(error);
                            };
                    }

                }),
                (error: any) => {
                    console.log(error);
                };
        }
    }

    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    onDrawerButtonTap(): void {
        this.drawerComponent.sideDrawer.showDrawer();
    }

    onStartTask(event: any): void {
        if(!this.getEnabled(event.index)) return;
        console.log("[Task number] " + event.index);
        this.taskService.tasks = this.tasks;
        this.taskService.currentTask = this.tasks[event.index];
        this.drawerComponent.sideDrawer.closeDrawer()
        this.router.navigate(["/instruction"]);
    }

    getTaskStatus(task: Task): string {
        return task.status.replace("_", " ");
    }

    getEnabled(i: number): boolean {
        return this.tasks[i].completionPercentage.toString() !== "100";
    }

    private getBlockStatus(i: number): string {
        let currentTask: Task = this.tasks[i];
        let invalidTask = !this.taskService.currentTask || this.taskService.currentTask.taskSpecification.code !== currentTask.taskSpecification.code || currentTask.status !== TaskStatus.STARTED;
        if (invalidTask) {
            return "";
        }
        let currentBlock: Block = this.taskService.getCurrentAcitveBlock();
        if (!currentBlock) {
            return "";
        }
        return currentBlock.status;

    }

    private getPopUpText(): string {
        return `Hello, ${this.user.name}
        This is your home page for the ORCA test battery. The ORCA test battery aims to measure your
        memory and learning abilities in a novel and interesting way. We have created two tests
        based on validated
        neuropsychological paradigms that we think you will enjoy. You will be required to log into
        your ORCA account everyday for 5 consecutive days to take these tests. We understand that
        this sounds tedious, but we hope that you will enjoy taking our tests so much that you won’t hesitate to
        come back! You might even find yourself recognizing some common Mandarin characters or
        Australasian bird songs at the end of the week!
        Please take note of your email address and password, as you will need these to access your
        account in the coming days. If you need to change your password, you can do so by clicking
        on “Profile”. You can also track your progress on each task in “Profile".
        Thank you for taking the ORCA test battery!`;
    }
}
