import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { HomeRoutingModule } from "./home-routing.module";
import { HomeComponent } from "./home.component";
import { NativeScriptUISideDrawerModule } from "nativescript-telerik-ui-pro/sidedrawer/angular";
import { DrawerComponent } from "../sub-component/drawer/drawer.component";
import { SubComponentModule } from "../sub-component/sub-component.module";

@NgModule({
    imports: [
        NativeScriptModule,
        HomeRoutingModule,
        NativeScriptUISideDrawerModule,
        SubComponentModule
    ],
    declarations: [
        HomeComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class HomeModule { }
