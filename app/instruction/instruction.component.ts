import { Component, OnInit, ViewChild, AfterViewChecked, OnDestroy } from "@angular/core";
import { DrawerTransitionBase, SlideInOnTopTransition } from "nativescript-telerik-ui-pro/sidedrawer";
import { RadSideDrawerComponent } from "nativescript-telerik-ui-pro/sidedrawer/angular";
import { UserService, User, SettingService, TaskService, Session, Block, TrialSpecification, TaskStatus } from "../shared";
import { Router } from "@angular/router";
import * as dialogs from "ui/dialogs";
import { PageRoute } from "nativescript-angular/router";
import { RouterExtensions } from "nativescript-angular";
import * as moment from "moment";
import { Subscription } from "rxjs";
import { TimerObservable } from "rxjs/observable/TimerObservable";

@Component({
    selector: "Instruction",
    moduleId: module.id,
    templateUrl: "./instruction.component.html"
})

export class InstructionComponent implements OnInit, OnDestroy {
    @ViewChild("drawer") drawerComponent: RadSideDrawerComponent;
    currentUser: User;
    txtAlert: string;
    tasks: any[];
    session: Session;
    currentBlock: Block;
    countDown: string;
    isAfter: boolean = false;
    earliestStartDateTime: moment.Moment = null;
    countTimeSubscription: Subscription;
    txtInstruction: string;
    txtInstructionEnd: string;

    private _sideDrawerTransition: DrawerTransitionBase;
    constructor(private userService: UserService, private router: RouterExtensions, private pageRoute: PageRoute, private taskService: TaskService) {
        this.session = this.taskService.currentTask.currentSession;
        this.txtInstruction = "When you are ready, press the Start button to begin the task. Remember, once you begin, you will need to complete today’s session in one go. This will take approximately 30 minutes.";
        this.txtInstructionEnd = "to complete the next session!";
    }
    ngOnDestroy() {
        if (this.countTimeSubscription)
            this.countTimeSubscription.unsubscribe();
    }
    ngOnInit(): void {
        this._sideDrawerTransition = new SlideInOnTopTransition();
        this.drawerComponent.sideDrawer.closeDrawer();
        this.setEarliestStartDateTime();
        let nowDateTime: number = moment.now();
        if (this.earliestStartDateTime && this.earliestStartDateTime.isValid) {
            this.isAfter = this.earliestStartDateTime.isAfter(nowDateTime);
            this.setCountDown();
        }
        dialogs.alert({
            title: "Instruction",
            message: this.getInstructionPart1(),
            okButtonText: "Ok"
        }).then(() => {
            dialogs.alert({
                title: "Instruction",
                message: this.getInstructionPart2(),
                okButtonText: "Ok"
            }).then(() => {
                dialogs.alert({
                    title: "Instruction",
                    message: this.getInstructionPart3(),
                    okButtonText: "Ok"
                }).then(() => {
                    console.log("Done alert");
                });
            });
        });
    }

    private setEarliestStartDateTime(): void {
        let currentBlock: Block = this.taskService.getCurrentAcitveBlock();
        this.earliestStartDateTime = currentBlock ? moment(currentBlock.earliestStartDateTime) : null;
    }

    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    private onDrawerButtonTap(): void {
        this.drawerComponent.sideDrawer.showDrawer();
    }

    private onStartTask(): void {
        if (this.isAfter) {
            dialogs.alert({
                title: "Instruction",
                message: "Pls wait",
                okButtonText: "Ok"
            }).then(() => {
                console.log("wait");
            });
            return;
        }
        else {
            dialogs.alert({
                title: "Instruction",
                message: "Ready to go?",
                okButtonText: "Ok"
            }).then(() => {
                this.userService.auth().subscribe(response => {
                    console.log(response);
                    this.startTask();
                }, (error) => {
                    dialogs.alert({
                        title: "Error",
                        message: "Failed on loading trail details",
                        okButtonText: "Ok"
                    }).then(() => {
                        this.router.navigate(["/"]);
                    });
                });
            });
        }
    }

    private startTask(): void {
        if (this.taskService.currentTask.status === TaskStatus.NOT_STARTED) {
            this.userService.commonPostHttpCall(this.taskService.currentTask.taskHref + "/startTask")
                .subscribe(startedTask => {
                    console.log("[Succssfull started task: ]" + this.taskService.currentTask.taskHref + "/startTask");
                    this.taskService.currentTask = this.taskService.createTask(startedTask);
                    console.log("Task Status: " + this.taskService.currentTask.status);
                    SettingService.currentTask = JSON.stringify(this.taskService.currentTask);
                    this.getSession();

                }, (error: any) => {
                    console.log(error);
                })
        }
        else {
            this.getSession();
        }
    }

    private getSession(): void {
        this.userService.commonGetHttpCall(this.taskService.currentTask.currentSessionHref)
            .subscribe(session => {
                console.log("[Succssfull get details of a session: ]" + this.taskService.currentTask.sessionHref);
                this.taskService.setSession(session);
                if (this.taskService.currentTask.currentSession.status === TaskStatus.NOT_STARTED) {
                    this.startSession();
                }
                else if (this.taskService.currentTask.currentSession.status === TaskStatus.COMPLETED && this.taskService.currentTask.transferHref) {
                    this.getTransfer();
                }
                else {
                    this.getBlock();
                }
            }),
            (error: any) => {
                console.log(error);
            };
    }

    private startSession(): void {
        this.userService.commonPostHttpCall(this.taskService.currentTask.currentSession.href + "/startSession")
            .subscribe(startedSession => {
                console.log("[Succssfull started a session]: " + this.taskService.currentTask.currentSession.href + "/startSession");
                this.taskService.setSession(startedSession);
                this.getBlock();
            }),
            (error: any) => {
                console.log(error);
            };
    }

    private getBlock(): void {
        this.userService.commonGetHttpCall(this.taskService.currentTask.currentSession.currentBlockHref)
            .subscribe(block => {
                console.log("[Succssfull get details of a block: ]" + this.taskService.currentTask.currentSession.currentBlockHref);
                this.taskService.setBlock(block);
                if (this.taskService.currentTask.currentSession.currentBlock.status === TaskStatus.NOT_STARTED) {
                    this.startBlock();
                }
                else {
                    this.router.navigate(["/home"], { clearHistory: true });
                }
            }),
            (error: any) => {
                console.log(error);
            };
    }

    private getTransfer(): void {
        this.userService.commonGetHttpCall(this.taskService.currentTask.transferHref)
            .subscribe(transfer => {
                console.log("[Succssfull get details of a transfer: ]" + this.taskService.currentTask.transferHref);
                this.taskService.setTransfer(transfer);
                if (this.taskService.currentTask.transfer.status === TaskStatus.NOT_STARTED) {
                    this.startTransfer();
                }
                else if (this.taskService.currentTask.transfer.status === TaskStatus.STARTED) {
                    this.startTransferTrailSpecificcation();
                }
                else {
                    this.router.navigate(["/home"], { clearHistory: true });
                }
            }),
            (error: any) => {
                console.log(error);
            };
    }

    private startTransfer(): void {
        this.userService.commonPostHttpCall(this.taskService.currentTask.transferHref + "/startTransfer").subscribe(startedTransfer => {
            console.log("[Succssfull started of a transfer: ]" + this.taskService.currentTask.currentSession.currentBlock.blockHref + "/startTransfer");
            this.taskService.setTransfer(startedTransfer);
            this.startTransferTrailSpecificcation();
        }),
            (error: any) => {
                console.log(error);
            };
    }

    private startBlock(): void {
        this.userService.commonPostHttpCall(this.taskService.currentTask.currentSession.currentBlock.blockHref + "/startBlock")
            .subscribe(startedBlock => {
                console.log("[Succssfull started of a block: ]" + this.taskService.currentTask.currentSession.currentBlock.blockHref + "/startBlock");
                this.taskService.setBlock(startedBlock);
                this.startTrailSpecificcation();
            }),
            (error: any) => {
                console.log(error);
            };
    }

    private startTrailSpecificcation(): void {
        this.userService.commonGetHttpCall(this.taskService.currentTask.currentSession.currentBlock.specificationHref)
            .subscribe(trialSpecification => {
                console.log("[Succssfull get details of specification: ]" + this.taskService.currentTask.currentSession.currentBlock.blockHref + "/startBlock");
                let currentTrialSpecification: TrialSpecification[] = this.taskService.createTrialSpecifications(trialSpecification.trialSpecifications)
                this.taskService.currentTask.currentSession.currentBlock.trailSpecifications = currentTrialSpecification;
                console.log("Block status: " + this.taskService.currentTask.currentSession.currentBlock);
                SettingService.currentTask = JSON.stringify(this.taskService.currentTask.currentSession.currentBlock);
                this.onNavigateToTask();
            }),
            (error: any) => {
                console.log(error);
            };
    }

    private startTransferTrailSpecificcation(): void {
        this.userService.commonGetHttpCall(this.taskService.currentTask.transfer.specificationHref)
            .subscribe(trialSpecification => {
                console.log("[Succssfull get details of specification: ]" + this.taskService.currentTask.transferHref + "/startTransfer");
                let currentTrialSpecification: TrialSpecification[] = this.taskService.createTrialSpecifications(trialSpecification.trialSpecifications)
                this.taskService.currentTask.transfer.trailSpecifications = currentTrialSpecification;
                console.log("Transfer status: " + this.taskService.currentTask.transfer);
                this.onNavigateToTask();
            }),
            (error: any) => {
                console.log(error);
            };
    }

    private onNavigateToTask(): void {
        this.drawerComponent.sideDrawer.closeDrawer();
        this.router.navigate(["/task"], { clearHistory: true });
    }

    private getInstructionPart1(): string {
        return `The aim of this task is to teach you to identify the bird songs of some common Australasian birds. A
        picture of the bird will be presented on the computer screen, and at the same time, you will hear a
        portion of bird song. You will learn which song matches which bird by seeing these matches
        repeatedly.`;
    }

    private getInstructionPart2(): string {
        return `Each time a bird appears on the screen and a song is heard, please respond intuitively and decide
        whether the song correctly matches the bird or not.Please try to be as accurate astrailSpecificationble, so be careful with your responses. Once you have decided,
        please press Yes or No as quickly as you can after the bird has disappeared from the screen.`

    }

    private getInstructionPart3(): string {
        return `Once you have made a response, you will hear a ‘beep’ sound. However, the computer will not tell you
        whether your response was right or wrong! If you take too long to respond, you will see “Time
        exceeded”, and the task will move onto the next bird.
        Once you hear the ‘beep’, a different bird will appear on the screen and this will be accompanied by
        a song. Once again, tap Yes or No button to indicate whether you think the song correctly matches the bird.`
    }

    private getText(): string {
        if (this.isAfter) {
            return `It is great that you are so enthusiastic, but you will need to wait until 
            ${this.countDown} 
            to complete the next session!`
        }
        return `When you are ready, press the Start button to begin the task. Remember, once you begin, you will need to complete today’s session in one go. This will take approximately 30 minutes.`;
    }

    private setCountDown(): void {
        if (this.isAfter) {
            this.txtInstruction = "It is great that you are so enthusiastic, but you will need to wait until ";
            let timer = TimerObservable.create(2000, 1000);
            this.countTimeSubscription = timer.subscribe(t => {
                let currentBlock: Block = this.taskService.getCurrentAcitveBlock();
                if (currentBlock && currentBlock.status === TaskStatus.NOT_STARTED) {
                    let latestCompletionDateTime: moment.Moment = moment(currentBlock.earliestStartDateTime);
                    let diff = latestCompletionDateTime.diff(moment.now());
                    let duration = moment.duration(diff);
                    if (duration.asMilliseconds() > 0) {
                        this.countDown = duration.minutes().toString() + " Minutes : " + duration.seconds().toString() + " Seconds";
                    }
                    else {
                        this.txtInstruction = "When you are ready, press the Start button to begin the task. Remember, once you begin, you will need to complete today’s session in one go. This will take approximately 30 minutes.";
                        this.txtInstructionEnd = "";
                    }
                }
            });
        }
    }
}
