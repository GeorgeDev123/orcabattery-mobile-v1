import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { InstructionRoutingModule } from "./instruction-routing.module";
import { InstructionComponent } from "./instruction.component";
import { NativeScriptUISideDrawerModule } from "nativescript-telerik-ui-pro/sidedrawer/angular";
import { DrawerComponent } from "../sub-component/drawer/drawer.component";
import { SubComponentModule } from "../sub-component/sub-component.module";

@NgModule({
    imports: [
        NativeScriptModule,
        InstructionRoutingModule,
        NativeScriptUISideDrawerModule,
        SubComponentModule
    ],
    declarations: [
        InstructionComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class InstructionModule { }
