import { Component, ElementRef, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { UserService, SettingService, User, Config, TaskService, Task } from "../shared/";
import { Router } from "@angular/router"
import { Page } from "ui/page";
import { Color } from "color";
import { View } from "ui/core/view";
// import { setHintColor } from "../../utils/hint-util";
import { TextField } from "ui/text-field";
import * as dialogs from "ui/dialogs";
import { RouterExtensions } from "nativescript-angular";
import { isAndroid, isIOS, device, screen } from "platform";

@Component({
    selector: "my-app",
    templateUrl: "login/login.component.html",
    styleUrls: ["login/login.component.css", "login/login.css"]
})
export class LoginComponent implements OnInit, AfterViewInit {
    @ViewChild("email") email: ElementRef;
    @ViewChild("password") password: ElementRef;
    @ViewChild("container") container: ElementRef;
    ngOnInit(): void {
        this.page.actionBarHidden = true;
        this.page.backgroundImage = "res://home_bg";
        if (SettingService.participantDetails) {
            this.user = JSON.parse(SettingService.participantDetails);
        }

    }
    user: User;
    isLoggingIn = true;
    constructor(private router: RouterExtensions, private page: Page, private userService: UserService, private taskService: TaskService) {
        this.user = new User();
        this.user.email = "";
        this.user.password = "";
    }
    ngAfterViewInit(): void {
        if (isIOS) {
            let textField: any = (<TextField>this.email.nativeElement).ios;
            textField.tintColor = new Color("white").ios;
        }
    }

    auth() {
        this.userService.auth().subscribe(
            () => {
                this.onLoginSuccessful();
            },
            (error: any) => {
                if (error.url.includes(Config.authentication)) {
                    if (error.headers.get("Set-Cookie")) {
                        var token = error.headers.get("Set-Cookie").split(";")[0].split("=")[1];
                        if (token) SettingService.token = token;
                        console.log("Token is: " + token);
                    }
                    this.login();
                }
                else {
                    alert("Unfortunately we could not find your account.");
                }
            }

        )
    }

    login() {
        this.userService.login(this.user).subscribe(res => {
            let token = res.headers.get("Set-Cookie").split(";")[2].split("=")[2];
            console.log("LOGIN OK " + token);
            SettingService.token = token;
            this.onLoginSuccessful();
        },
            (error: any) => {
                alert("Unfortunately we could not find your account.");
            }

        )
    }
    onLoginSuccessful(): void {
        this.userService.mainApi().subscribe(res => {
            let profilehref = res._links.profile.href;
            this.userService.profile(profilehref).subscribe(res => {
                SettingService.participantUrl = profilehref;
                let existingUser: User = this.user;
                existingUser.name = res.firstName + " " + res.lastName;
                existingUser.memberNumber = profilehref.split("participants/")[1];
                SettingService.participantDetails = JSON.stringify(existingUser);
                var userJson = JSON.stringify(this.user);
                SettingService.participantDetails = userJson;
                let tasksUrl: any[] = res._links.tasks;
                SettingService.tasksUrl = JSON.stringify(tasksUrl);
                dialogs.alert({
                    title: "Login",
                    message: "Login successful",
                    okButtonText: "Ok"
                }).then(() => {
                    this.router.navigate(["/home"], { clearHistory: true });
                });
            })
        }),
            (error) => {
                dialogs.alert({
                    title: "Error",
                    message: "Failed on loading participant details",
                    okButtonText: "Ok"
                }).then(() => {
                    this.router.navigate(["/"]);
                });
            }
    }
    toggleDisplay() {
        this.isLoggingIn = !this.isLoggingIn;
    }
}