import { Component, OnInit, ViewChild } from "@angular/core";
import { DrawerTransitionBase, SlideInOnTopTransition } from "nativescript-telerik-ui-pro/sidedrawer";
import { RadSideDrawerComponent } from "nativescript-telerik-ui-pro/sidedrawer/angular";
import { UserService, User, SettingService, TrailResponse, TaskStatus, ResponseType, TaskService, ResponseChart } from "../shared";
import { Router } from "@angular/router";
import * as dialogs from "ui/dialogs";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";

@Component({
    selector: "Profile",
    moduleId: module.id,
    templateUrl: "./profile.component.html"
})
export class ProfileComponent implements OnInit {
    @ViewChild("drawer") drawerComponent: RadSideDrawerComponent;
    private _responseSource: ObservableArray<ResponseChart>;
    private _sideDrawerTransition: DrawerTransitionBase;
    private anyBlock: boolean = false;
    constructor(private userService: UserService, private router: Router, private taskService: TaskService) {

    }

    ngOnInit(): void {
        console.log("Intial the Profile componet");
        this.getAllCompletedBlocks();

        this._sideDrawerTransition = new SlideInOnTopTransition();
        this.drawerComponent.sideDrawer.closeDrawer();
    }

    get responseSource(): ObservableArray<ResponseChart> {
        return this._responseSource;
    }


    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    getAllCompletedBlocks() {
        let tasks: any[] = JSON.parse(SettingService.tasks);
        for (var i = 0; i < tasks.length; i++) {

        }
        let sessionHrefs: string[] = tasks.map(task => {
            return task._links.sessions ? task._links.sessions.href : "";
        });
        if (sessionHrefs && sessionHrefs.length > 0) {
            this.userService.joinGet(sessionHrefs).subscribe(sessions => {
                let blockHrefsObj: any[] = sessions.map(session => {
                    let blocks: any[] = [];
                    if (session._links && session._links.blocks && session._links.blocks.constructor === Array) {
                        blocks = session._links.blocks;
                    }
                    else if (session._links && session._links.blocks) {
                        blocks.push(session._links.blocks);
                    }
                    return blocks.map(block => {
                        return block.href;
                    })
                });
                let blockHrefs: string[] = blockHrefsObj.reduce((a, b) => {
                    return a.concat(b);
                })
                if (blockHrefs && blockHrefs.length > 0) {
                    this.userService.joinGet(blockHrefs).subscribe(blocks => {
                        let trialsObj = blocks.map(block => {
                            return block.trials
                        });
                        if (trialsObj && trialsObj.length > 0) {
                            let trials: any[] = trialsObj.reduce((a, b) => {
                                return a.concat(b);
                            })
                            let responses: TrailResponse[] = this.taskService.createHistoryTrailResponses(trials);
                            this._responseSource = new ObservableArray(this.getResponseSource(responses));

                            this.anyBlock = this._responseSource.length > 0 ? true : false;
                        }
                    });
                }
            })
        }
    }

    getResponseSource(responses: TrailResponse[]): ResponseChart[] {
        let displayReponses: ResponseChart[] = new Array<ResponseChart>();
        for (var i = 0; i < responses.length; i++) {
            let newDisplayResponse: ResponseChart = new ResponseChart({
                response: responses[i].response,
                auditoryStimulusFilename: responses[i].auditoryStimulusFilename,
                visualStimulusFilename: responses[i].visualStimulusFilename,
                index: i
            })

            if (responses[i].response === ResponseType[ResponseType.TIMED_OUT]) {
                newDisplayResponse.responseTimeInSeconds = 0;
            }
            else {
                newDisplayResponse.responseTimeInSeconds = responses[i].responseTimeInSeconds;
            }
            displayReponses.push(newDisplayResponse);
        }
        return displayReponses;
    }

    onDrawerButtonTap(): void {
        this.drawerComponent.sideDrawer.showDrawer();
    }
}