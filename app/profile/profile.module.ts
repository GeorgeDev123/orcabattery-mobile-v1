import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { ProfileRoutingModule } from "./profile-routing.module";
import { ProfileComponent } from "./profile.component";
import { NativeScriptUISideDrawerModule } from "nativescript-telerik-ui-pro/sidedrawer/angular";
import { DrawerComponent } from "../sub-component/drawer/drawer.component";
import { SubComponentModule } from "../sub-component/sub-component.module";
import { NativeScriptUIChartModule } from "nativescript-telerik-ui-pro/chart/angular";

@NgModule({
    imports: [
        NativeScriptModule,
        ProfileRoutingModule,
        NativeScriptUISideDrawerModule,
        SubComponentModule,
        NativeScriptUIChartModule
    ],
    declarations: [
        ProfileComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ProfileModule { }
