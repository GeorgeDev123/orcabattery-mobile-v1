export class Config {
    static apiUrl = "https://dev.orcabattery.org.au/api";
    static token = "";
    static authentication = "/authentication";
    static signup = "/participant";
    static login = "/login";
    static lgoout = "/logout";
    static profile = "/participants";
    static participant = "/participant";
  }