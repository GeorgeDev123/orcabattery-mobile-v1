import { getString, setString } from "application-settings";
import { Injectable } from "@angular/core";

@Injectable()
export class SettingService {
    public static get xsrf(): string {
        return getString("xsrf");
    }
    public static set token(theToken: string) {
        setString("xsrf", theToken);
    }
    public static get participantUrl(): string {
        return getString("participantUrl");
    }
    public static set participantUrl(href: string) {
        setString("participantUrl", href);
    }
    public static get profile(): string {
        return getString("profile");
    }
    public static set profile(href: string) {
        setString("profile", href);
    }
    public static get participantDetails(): string {
        return getString("participantDetails");
    }
    public static set participantDetails(href: string) {
        setString("participantDetails", href);
    }
    public static get tasks(): string {
        return getString("tasks");
    }
    public static set tasks(href: string) {
        setString("tasks", href);
    }
    public static get currentTask(): string {
        return getString("currentTask");
    }
    public static set currentTask(href: string) {
        setString("currentTask", href);
    }
    public static get history(): string {
        return getString("history");
    }
    public static set history(href: string) {
        setString("history", href);
    }
    public static get tasksUrl(): string {
        return getString("tasksUrl");
    }
    public static set tasksUrl(href: string) {
        setString("tasksUrl", href);
    }
}