import { Injectable } from "@angular/core";
import { Session, Task, TaskSpecification, TaskStatus, Block, TrialSpecification, TrailResponse, ResponseChart, ResponseType, TransferResponseType } from "./trial";
import { getString, setString } from "application-settings";

@Injectable()
export class TaskService {
    tasks: Task[];
    currentTask: Task;
    public createTasks(taskJson: any[]): Task[] {
        return taskJson.map(taskJson => {
            return this.createTask(taskJson);
        });
    }
    public createTask(taskJson: any): Task {
        let taskSpecificationJsonObj: any = taskJson.specification;
        let taskSpecification: TaskSpecification = new TaskSpecification({
            code: taskSpecificationJsonObj.code,
            name: taskSpecificationJsonObj.name,
            description: taskSpecificationJsonObj.description,
            minimumInterSessionIntervalInHours: taskSpecificationJsonObj.minimumInterSessionIntervalInHours,
            maximumInterSessionIntervalInHours: taskSpecificationJsonObj.maximumInterSessionIntervalInHours,
            minimumPostSessionsTransferIntervalInMinutes: taskSpecificationJsonObj.minimumPostSessionsTransferIntervalInMinutes,
            maximumPostSessionsTransferIntervalInMinutes: taskSpecificationJsonObj.maximumPostSessionsTransferIntervalInMinutes,
            postTransferSubmissionWindowInMinutes: taskSpecificationJsonObj.postTransferSubmissionWindowInMinutes
        });
        let task: Task = new Task({
            taskHref: taskJson._links.self.href,
            taskStartHref: taskJson._links.startTask ? taskJson._links.startTask.href : "",
            taskSpecification: taskSpecification,
            currentSessionHref: taskJson._links.sessions ? taskJson._links.sessions.href : "",
            startDateTime: taskJson.startDateTime,
            exitDateTime: taskJson.exitDateTime,
            completionDateTime: taskJson.completionDateTime,
            status: taskJson.status,
            completionPercentage: Number(taskJson.completionPercentage),
            transferHref: taskJson._links.transfer ? taskJson._links.transfer.href : ""
        });
        return task;
    }

    public createSession(session: any): Session {
        let sessionSepecificationJsonObj: any = session.specification;
        let currentSession: Session = new Session({
            href: session._links.self.href,
            minimumInterBlockIntervalInMinutes: Number(sessionSepecificationJsonObj.minimumInterBlockIntervalInMinutes),
            maximumInterBlockIntervalInMinutes: Number(sessionSepecificationJsonObj.maximumInterBlockIntervalInMinutes),
            postBlockSubmissionWindowInMinutes: Number(sessionSepecificationJsonObj.postBlockSubmissionWindowInMinutes),
            blockRepetitions: Number(sessionSepecificationJsonObj.blockRepetitions),
            earliestStartDateTime: session.earliestStartDateTime,
            latestStartDateTime: session.latestStartDateTime,
            latestCompletionDateTime: session.latestCompletionDateTime,
            startDateTime: session.startDateTime,
            exitStartDate: session.exitStartDate,
            completionDateTime: session.completionDateTime,
            status: session.status,
            currentBlockHref: session._links && session._links.nextBlock ? session._links.nextBlock.href : session._links && session._links.currentBlock ? session._links.currentBlock.href : ""
        });
        return currentSession;
    }

    public setSession(newSessionJson: any): void {
        let currentSession = this.createSession(newSessionJson);
        this.addOrUpdateSessions(currentSession);
        this.currentTask.currentSession = currentSession;
        console.log("Session status: " + this.currentTask.currentSession.status);
        this.updateCurrentTask();
    }

    public addOrUpdateSessions(newSession: Session): Task {
        if (!this.currentTask.sessions || this.currentTask.sessions.length <= 0) {
            this.currentTask.sessions = new Array<Session>();
            this.currentTask.sessions.push(newSession);
            return this.currentTask;
        }
        else {
            let sessionIndex = this.currentTask.sessions.findIndex(session => {
                return session.href === newSession.href;
            });
            if (sessionIndex !== -1) {
                this.currentTask.sessions[sessionIndex] = newSession;
            }
            else {
                this.currentTask.sessions.push(newSession);
            }
        }
        return this.currentTask;
    }

    public createBlock(block: any, isTranfer: boolean = false, trails?: TrailResponse[]): Block {
        let bLockSpecificationJsonObj: any = block.specification;
        let currentBlock: Block = new Block({
            blockHref: block._links.self.href,
            auditoryStimulusDurationInSeconds: Number(bLockSpecificationJsonObj.auditoryStimulusDurationInSeconds),
            visualStimulusDurationInSeconds: Number(bLockSpecificationJsonObj.visualStimulusDurationInSeconds),
            timeoutNoticeDurationInSeconds: Number(bLockSpecificationJsonObj.timeoutNoticeDurationInSeconds),
            responseWindowInSeconds: Number(bLockSpecificationJsonObj.responseWindowInSeconds),
            interTrialIntervalInSeconds: Number(bLockSpecificationJsonObj.interTrialIntervalInSeconds),
            maximumDurationInSeconds: Number(bLockSpecificationJsonObj.maximumDurationInSeconds),
            specificationHref: bLockSpecificationJsonObj._links.generateTrialSpecifications.href,
            trailSpecifications: bLockSpecificationJsonObj.trails,
            earliestStartDateTime: block.earliestStartDateTime,
            latestStartDateTime: block.latestStartDateTime,
            latestCompletionDateTime: block.latestCompletionDateTime,
            startDateTime: block.startDateTime,
            exitDateTime: block.exitDateTime,
            completionDateTime: block.completionDateTime,
            status: block.status,
            isTransfer: isTranfer
        });
        if (trails) {
            currentBlock.trails = trails;
        }
        return currentBlock;
    }

    public setBlock(newBlockJson: any): void {
        let newBlock: Block = this.createBlock(newBlockJson, true);
        this.currentTask.currentSession.currentBlock = newBlock;
        this.addOrUpdateSessions(this.currentTask.currentSession);
        console.log("Block status: " + this.currentTask.currentSession.currentBlock.status);
        this.updateCurrentTask();
    }

    public setTransfer(newBlockJson: any): void {

        let newTransfer: Block = this.createBlock(newBlockJson, true);
        this.currentTask.transfer = newTransfer;
        console.log("Transfer status: " + this.currentTask.transfer.status);
        this.updateCurrentTask();
    }

    public createTrailResponses(trailResponses: any[], isTranfer: boolean = false): TrailResponse[] {
        return trailResponses.map(trailResponse => {
            return this.createTrailResponse(trailResponse, isTranfer);
        })
    }

    public createTrailResponse(trailResponse: any, isTransfer: boolean): TrailResponse {
        let currentTrailResponse: TrailResponse = new TrailResponse({
            visualStimulusFilename: trailResponse.visualStimulusFilename,
            response: trailResponse.response
        })
        if (!isTransfer) {
            currentTrailResponse.auditoryStimulusFilename = trailResponse.auditoryStimulusFilename;
        }
        if (trailResponse.responseTimeInSeconds) { currentTrailResponse.responseTimeInSeconds = Number(trailResponse.responseTimeInSeconds); }
        return currentTrailResponse;
    }

    public createTrialSpecifications(trialSpecification: any[], isTransfer: boolean = false): TrialSpecification[] {
        return trialSpecification.map(trialSpecification => {
            return this.createTrialSpecification(trialSpecification, isTransfer);
        })
    }

    public createTrialSpecification(trialSpecification: any, isTransfer: boolean = false): TrialSpecification {
        let currentTrailTaskSpecification: TrialSpecification = new TrialSpecification({
            visualStimulusFilename: trialSpecification.visualStimulusFilename
        });
        if (!isTransfer) {
            currentTrailTaskSpecification.auditoryStimulusFilename = trialSpecification.auditoryStimulusFilename;
        }
        return currentTrailTaskSpecification;
    }

    public updateCurrentTask(): void {
        setString("currentTask", JSON.stringify(this.currentTask));
    }

    public getCurrentAcitveBlock(): Block {
        let currentBlock: Block = null;
        if (!this.currentTask) return currentBlock;
        if (!this.currentTask.currentSession) return currentBlock;
        if (!this.currentTask.currentSession.currentBlock) {
            if (this.currentTask.currentSession.status === TaskStatus.COMPLETED && this.currentTask.transfer) {
                return this.currentTask.transfer;
            }
            return currentBlock;
        }

        if (this.currentTask.currentSession.currentBlock.status !== TaskStatus.COMPLETED.toString() ||
            this.currentTask.currentSession.currentBlock.status !== TaskStatus.EXITED.toString()) {
            return this.currentTask.currentSession.currentBlock;
        }
        return currentBlock;
    }

    public createHistoryTrailResponses(trailResponses: any[], isTranfer: boolean = false): TrailResponse[] {
        return trailResponses.map(trailResponse => {
            return this.createTrailResponse(trailResponse, isTranfer);
        })
    }

    public createHistoryTrailResponse(trailResponse: any, isTransfer: boolean): TrailResponse {
        let currentTrailResponse: TrailResponse = new TrailResponse({
            visualStimulusFilename: trailResponse.visualStimulusFilename,
            response: trailResponse.response
        })
        if (!isTransfer) {
            currentTrailResponse.auditoryStimulusFilename = trailResponse.auditoryStimulusFilename;
        }
        if (trailResponse.responseTimeInSeconds) { currentTrailResponse.responseTimeInSeconds = Number(trailResponse.responseTimeInSeconds); }
        return currentTrailResponse;
    }

    public getTrailResponseSource(responses: TrailResponse[]): ResponseChart[] {
        let displayReponses: ResponseChart[] = new Array<ResponseChart>();
        for (var i = 0; i < responses.length; i++) {
            let newDisplayResponse: ResponseChart = new ResponseChart({
                response: responses[i].response,
                auditoryStimulusFilename: responses[i].auditoryStimulusFilename,
                visualStimulusFilename: responses[i].visualStimulusFilename,
                index: i
            })

            if (responses[i].response === ResponseType[ResponseType.TIMED_OUT]) {
                newDisplayResponse.responseTimeInSeconds = 0;
            }
            else {
                newDisplayResponse.responseTimeInSeconds = responses[i].responseTimeInSeconds;
            }
            displayReponses.push(newDisplayResponse);
        }
        return displayReponses;
    }

    getTransferResponseSource(responses: TrailResponse[]): ResponseChart[] {
        let displayReponses: ResponseChart[] = new Array<ResponseChart>();
        for (var i = 0; i < responses.length; i++) {
            let newDisplayResponse: ResponseChart = new ResponseChart({
                response: responses[i].response,
                visualStimulusFilename: responses[i].visualStimulusFilename,
                index: i
            })

            if (responses[i].response === TransferResponseType[TransferResponseType.TIMED_OUT]) {
                newDisplayResponse.responseTimeInSeconds = 0;
            }
            else {
                newDisplayResponse.responseTimeInSeconds = responses[i].responseTimeInSeconds;
            }
            displayReponses.push(newDisplayResponse);
        }
        return displayReponses;
    }
}