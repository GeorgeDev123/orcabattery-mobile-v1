export class Task {
    taskHref: string;
    taskStartHref: string;
    taskSpecification: TaskSpecification;
    currentSessionHref: string;
    currentSession: Session;
    startDateTime: any;
    exitDateTime: any;
    completionDateTime: any;
    status: string;
    completionPercentage: number;
    sessionHref: string;
    sessions: Session[] = new Array<Session>();
    transferHref: string;
    transfer: Block;
    constructor(init?: Partial<Task>) {
        Object.assign(this, init);
    }
}

export class TaskVM {
    name: string;
    status: string;
    completionPercentage: number;
    isCurrentBolck: boolean;
    blockStatus: string;
    countDown: string;
    constructor(init?: Partial<TaskVM>) {
        Object.assign(this, init);
    }
}

export enum TaskStatus {
    NOT_STARTED = "NOT_STARTED",
    STARTED = "STARTED",
    EXITED = "EXITED",
    COMPLETED = "COMPLETED"
}

export class TaskSpecification {
    code: string;
    name: string;
    description: string;
    minimumInterSessionIntervalInHours: number;
    maximumInterSessionIntervalInHours: number;
    minimumPostSessionsTransferIntervalInMinutes: number;
    maximumPostSessionsTransferIntervalInMinutes: number;
    postTransferSubmissionWindowInMinutes: number;
    constructor(init?: Partial<TaskSpecification>) {
        Object.assign(this, init);
    }
}

export class Session {
    href: string;
    minimumInterBlockIntervalInMinutes: number;
    maximumInterBlockIntervalInMinutes: number;
    postBlockSubmissionWindowInMinutes: number;
    blockRepetitions: number;
    blocks: Block[] = new Array<Block>();
    currentBlockHref: string;
    currentBlock: Block;
    earliestStartDateTime: string;
    latestStartDateTime: string;
    latestCompletionDateTime: string;
    startDateTime: string;
    exitStartDate: string;
    completionDateTime: string;
    status: string;
    constructor(init?: Partial<Session>) {
        Object.assign(this, init);
    }
}

export class Block {
    blockHref: string;
    auditoryStimulusDurationInSeconds: number;
    visualStimulusDurationInSeconds: number;
    timeoutNoticeDurationInSeconds: number;
    responseWindowInSeconds: number;
    interTrialIntervalInSeconds: number;
    maximumDurationInSeconds: number;
    specificationHref: string;
    trailSpecifications: TrialSpecification[] = new Array<TrialSpecification>();
    trails: TrailResponse[] = new Array<TrailResponse>();
    earliestStartDateTime: string;
    latestStartDateTime: string;
    latestCompletionDateTime: string;
    startDateTime: string;
    exitDateTime: string;
    completionDateTime: string;
    status: string;
    isTransfer: boolean;
    constructor(init?: Partial<Block>) {
        Object.assign(this, init);
    }
}

export class TrialSpecification {
    auditoryStimulusFilename: string;
    visualStimulusFilename: string;
    constructor(init?: Partial<TrialSpecification>) {
        Object.assign(this, init);
    }
}
export class TrailResponse {
    response: string;
    auditoryStimulusFilename: string;
    visualStimulusFilename: string;
    responseTimeInSeconds: number;
    constructor(init?: Partial<TrailResponse>) {
        Object.assign(this, init);
    }
}
export enum ResponseType {
    TIMED_OUT,
    MATCH,
    NON_MATCH
}

export enum TransferResponseType {
    NOVEL,
    FAMILIAR,
    TIMED_OUT
}
export class ResponseChart {
    response: string;
    auditoryStimulusFilename: string;
    visualStimulusFilename: string;
    responseTimeInSeconds: number;
    index; number;
    constructor(init?: Partial<ResponseChart>) {
        Object.assign(this, init);
    }
}