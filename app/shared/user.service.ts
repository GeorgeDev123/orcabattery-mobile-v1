import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions, URLSearchParams } from "@angular/http";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";

import { getString, setString } from "application-settings";
import { User, Config, TrialSpecification } from "./";

@Injectable()
export class UserService {
    constructor(private http: Http) { }

    register(user: User) {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");

        return this.http.post(
            Config.apiUrl + "Users",
            JSON.stringify({
                Username: user.email,
                Email: user.email,
                Password: user.password
            }),
            { headers: headers }
        )
            .catch(this.handleErrors);
    }

    handleErrors(error: Response) {
        console.log(JSON.stringify(error.json()));
        return Observable.throw(error);
    }
    auth() {
        let options = new RequestOptions({
            // withCredentials: true
        });
        return this.http.get(Config.apiUrl + Config.authentication, options).map(response => response.json())
            .catch(this.handleErrors);
    }
    login(user: User) {
        var token = getString("xsrf")
        const headerDict = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'X-XSRF-TOKEN': token,
        }
        const headerObj = {
            headers: new Headers(headerDict),
        };
        let body = new URLSearchParams();
        body.set('username', user.email);
        body.set('password', user.password);

        return this.http.post(
            Config.apiUrl + Config.login,
            body,
            this.getHeaderWithCredential()
        ).catch(this.handleErrors);
    }
    mainApi() {
        var token = getString("xsrf")
        const headerDict = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'X-XSRF-TOKEN': token,
        }
        const headerObj = {
            headers: new Headers(headerDict),
        };
        return this.http.get(
            Config.apiUrl,
            // this.getHeaderWithCredential()
        ).map(response => response.json()).catch(this.handleErrors);
    }
    profile(profileHref: string) {
        var token = getString("xsrf")
        const headerDict = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'X-XSRF-TOKEN': token,
        }
        const headerObj = {
            headers: new Headers(headerDict),
        };
        return this.http.get(
            profileHref,
            this.getHeaderWithCredential()
        ).map(response => response.json()).catch(this.handleErrors);
    }
    tasks(taskHref: any[]) {
        let taskHttpTasks = taskHref.map(task => {
            return this.http.get(task.href, this.getHeaderWithCredential()).map(res => res.json());
        })
        return Observable.forkJoin(taskHttpTasks);
    }

    joinGet(hrefs: string[]) {
        let httpsHrefs = hrefs.map(href => {
            return this.http.get(href, this.getHeaderWithCredential()).map(res => res.json());
        })
        return Observable.forkJoin(httpsHrefs);
    }

    session(sessionHref: string) {
        var headers = this.getHeader();
        return this.http.get(sessionHref, this.getHeaderWithCredential()).map(response => response.json()).catch(this.handleErrors);
    }

    commonGetHttpCall(href: string) {
        var headers = this.getHeader();
        return this.http.get(href, this.getHeaderWithCredential()).map(response => response.json()).catch(this.handleErrors);
    }
    commonPostHttpCall(href: string) {
        return this.http.post(
            href,
            {},
            this.getJsonHeader()
        ).map(response => JSON.parse(response.text())).catch(this.handleErrors);
    }
    startTask(startTaskHref) {
        return this.http.post(
            startTaskHref,
            {},
            {}
        ).catch(this.handleErrors);
    }

    private getTokenHeader(): any {
        var token = getString("xsrf")
        const headerDict = {
            'X-XSRF-TOKEN': token,
        }
        const headerObj = {
            headers: new Headers(headerDict),
        };
        return headerObj;
    }

    private getHeader(): any {
        var token = getString("xsrf")
        const headerDict = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'X-XSRF-TOKEN': token,
        }
        const headerObj = {
            headers: new Headers(headerDict)
        };
        return headerObj;
    }

    private getHeaderWithCredential(): any {
        var token = getString("xsrf")
        const headerDict = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'X-XSRF-TOKEN': token,
        }
        const headerObj = {
            headers: new Headers(headerDict),
            withCredentials: true
        };
        return headerObj;
    }

    private getJsonHeader(): any {
        var token = getString("xsrf")
        const headerDict = {
            'Content-Type': 'application/json',
            'X-XSRF-TOKEN': token,
        }
        const headerObj = {
            headers: new Headers(headerDict),
        };
        return headerObj;
    }

    public postTrailsResponses(completedBlockHref: string, trailSpecification: TrialSpecification[]) {
        return this.http.post(
            completedBlockHref,
            {
                trials: trailSpecification
            },
            this.getJsonHeader()).map(response => JSON.parse(response.text())).catch(this.handleErrors);
    }

    public getAllCompletedBlocks(){
        let tasks: any[] = JSON.parse(getString("tasks"));
        let sessionHrefs: string[] = tasks.map(task => {
            return task._links.sessions ? task._links.sessions.href : "";
        })
        if (sessionHrefs && sessionHrefs.length > 0) {
            this.joinGet(sessionHrefs).subscribe(sessions => {
                let blockHrefsObj: any[] = sessions.map(session => {
                    let blocks: any[] = session._links.blocks ? session._links.blocks : [];
                    return blocks.map(block => {
                        return block.href;
                    })
                });
                let blockHrefs: string[] = blockHrefsObj.reduce((a,b) => {
                    return a.concat(b);
                })
                if (blockHrefs && blockHrefs.length > 0) {
                    this.joinGet(blockHrefs).subscribe(blocks => {
                        return blocks;
                    });
                }
                else {
                    return [];
                }
            })
        }
        else {
            return [];
        }
    }
}