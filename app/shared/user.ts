import { Task } from "./trial";

export class User {
    email: string;
    password: string;
    name: string;
    memberNumber: string;
    tasks: Task[];
    // isValidEmail() {
    //   return validator.validate(this.email);
    // }
    constructor(init?:Partial<User>){
      Object.assign(this, init);
    }
  }