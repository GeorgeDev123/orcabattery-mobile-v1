import { Component, OnInit, ViewChild } from "@angular/core";
import { DrawerTransitionBase, SlideInOnTopTransition } from "nativescript-telerik-ui-pro/sidedrawer";
import { RadSideDrawerComponent } from "nativescript-telerik-ui-pro/sidedrawer/angular";
import { UserService, User, SettingService, TrailResponse, TaskStatus, ResponseType, TaskService, ResponseChart } from "../shared";
import { Router } from "@angular/router";
import * as dialogs from "ui/dialogs";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";

@Component({
    selector: "StatTransfer",
    moduleId: module.id,
    templateUrl: "./statTransfer.component.html"
})
export class StatTransferComponent implements OnInit {
    @ViewChild("drawer") drawerComponent: RadSideDrawerComponent;
    private _responseSource: ObservableArray<ResponseChart>;
    private _sideDrawerTransition: DrawerTransitionBase;
    private anyTransfer: boolean = false;
    constructor(private userService: UserService, private router: Router, private taskService: TaskService) {

    }

    ngOnInit(): void {
        console.log("Intial the StatTransfer componet");
        this.getAllCompletedBlocks();

        this._sideDrawerTransition = new SlideInOnTopTransition();
        this.drawerComponent.sideDrawer.closeDrawer();
    }

    get responseSource(): ObservableArray<ResponseChart> {
        return this._responseSource;
    }


    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    getAllCompletedBlocks() {
        let tasks: any[] = JSON.parse(SettingService.tasks);
        let transferHrefs: string[] = tasks.map(task => {
            return task._links.transfer ? task._links.transfer.href : "";
        });
        
        if (transferHrefs && transferHrefs.length > 0) {
            this.userService.joinGet(transferHrefs).subscribe(transfers => {
                let trialsObj: any[] = transfers.map(transfer => {
                    return transfer.trials;
                });
                if (trialsObj && trialsObj.length > 0) {
                    let trials: any[] = trialsObj.reduce((a, b) => {
                        return a.concat(b);
                    });
                    let responses: TrailResponse[] = this.taskService.createHistoryTrailResponses(trials, true);
                    this._responseSource = new ObservableArray(this.taskService.getTransferResponseSource(responses));
                    if(this._responseSource.length > 0) {
                        this.anyTransfer = true;
                    }
                }
            })
        }
    }

    getResponseSource(responses: TrailResponse[]): ResponseChart[] {
        let displayReponses: ResponseChart[] = new Array<ResponseChart>();
        for (var i = 0; i < responses.length; i++) {
            let newDisplayResponse: ResponseChart = new ResponseChart({
                response: responses[i].response,
                auditoryStimulusFilename: responses[i].auditoryStimulusFilename,
                visualStimulusFilename: responses[i].visualStimulusFilename,
                index: i
            })

            if (responses[i].response === ResponseType[ResponseType.TIMED_OUT]) {
                newDisplayResponse.responseTimeInSeconds = 0;
            }
            else {
                newDisplayResponse.responseTimeInSeconds = responses[i].responseTimeInSeconds;
            }
            displayReponses.push(newDisplayResponse);
        }
        return displayReponses;
    }

    onDrawerButtonTap(): void {
        this.drawerComponent.sideDrawer.showDrawer();
    }
}
