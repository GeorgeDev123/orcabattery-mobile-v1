import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { StatTransferRoutingModule } from "./statTransfer-routing.module";
import { StatTransferComponent } from "./statTransfer.component";
import { NativeScriptUISideDrawerModule } from "nativescript-telerik-ui-pro/sidedrawer/angular";
import { DrawerComponent } from "../sub-component/drawer/drawer.component";
import { SubComponentModule } from "../sub-component/sub-component.module";
import { NativeScriptUIChartModule } from "nativescript-telerik-ui-pro/chart/angular";

@NgModule({
    imports: [
        NativeScriptModule,
        StatTransferRoutingModule,
        NativeScriptUISideDrawerModule,
        SubComponentModule,
        NativeScriptUIChartModule
    ],
    declarations: [
        StatTransferComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class StatTransferModule { }
