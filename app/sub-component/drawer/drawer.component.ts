import { Component, Input, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { ItemEventData } from "ui/list-view";
import { User, SettingService } from "../../shared";

/* ***********************************************************
* Keep data that is displayed in your app drawer in the MyDrawer component class.
* Add new data objects that you want to display in the drawer here in the form of properties.
*************************************************************/
@Component({
    selector: "MyDrawer",
    moduleId: module.id,
    templateUrl: "./drawer.component.html",
    styleUrls: ["./drawer.component.css", "./drawer.css"],
    providers: [SettingService]
})
export class DrawerComponent implements OnInit {
    @Input() selectedPage: string;

    private _navigationItems: Array<any>;
    private user: User;
    constructor(private routerExtensions: RouterExtensions) {
        this.user = JSON.parse(SettingService.participantDetails);
    }

    ngOnInit(): void {
        this._navigationItems = [
            {
                title: "Home",
                name: "home",
                route: "/home",
                icon: "\uf015"
            },
            {
                title: "Transfer Statistics",
                name: "statTransfer",
                route: "/statTransfer",
                icon: "\uf002"
            },
            {
                title: "Block Statistics",
                name: "profile",
                route: "/profile",
                icon: "\uf005"
            }
        ];
    }

    get navigationItems(): Array<any> {
        return this._navigationItems;
    }

    onNavigationItemTap(args: ItemEventData): void {
        if (this.isPageSelected(args.view.bindingContext.title)) return;
        const navigationItemView = args.view;
        const navigationItemRoute = navigationItemView.bindingContext.route;

        this.routerExtensions.navigate([navigationItemRoute], {
            transition: {
                name: "slide"
            }
        });
    }

    isPageSelected(pageTitle: string): boolean {
        return pageTitle === this.selectedPage;
    }
}
