import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { UserService, SettingService, User, Config, TaskService } from "../shared/";
import { Router } from "@angular/router"
import { Page } from "ui/page";
import { Color } from "color";
import { View } from "ui/core/view";
import { Button } from "ui/button";
import { Image } from "ui/image";
import { setInterval, setTimeout, clearInterval } from "timer";
import { TNSPlayer, AudioPlayerOptions } from 'nativescript-audio';
import { alert } from "ui/dialogs";
import * as moment from "moment";
import * as Rx from "rxjs/Rx";
// import { setHintColor } from "../../utils/hint-util";
import { TextField } from "ui/text-field";
import * as dialogs from "ui/dialogs";
import { Observable } from "tns-core-modules/data/observable/observable";
import { TrailResponse, ResponseType, TrialSpecification, Task, TaskStatus, TransferResponseType } from '../shared/';
import { RouterExtensions } from "nativescript-angular";
import * as LocalNotifications from "nativescript-local-notifications";

@Component({
    selector: "my-app",
    templateUrl: "task/task.component.html",
    styleUrls: ["task/task.component.css", "task/task.css"]
})
export class TaskComponent implements OnInit {
    @ViewChild("btnNo") btnNo: ElementRef;
    @ViewChild("btnYes") btnYes: ElementRef;
    @ViewChild("img") img: ElementRef;
    public isPlaying: boolean;
    private player: TNSPlayer;

    user: User;
    isLoggingIn = true;
    public id;
    public timeout;
    private blockSpecification: TrialSpecification[];
    private turnIndex: number = 0;
    private audioBeepPath: string = "~/static/";
    private audioPath: string = "~/static/auditory-stimulus/bird-songs/";
    private visualPath: string = "~/static/visual-stimulus/bird-songs/";
    private responses: TrailResponse[] = new Array<TrailResponse>();
    private currentTurnInputStart: moment.Moment;
    private showImage: boolean;
    private isTransfer: boolean = false;

    constructor(private router: RouterExtensions, private page: Page, private userService: UserService, private taskService: TaskService) {
        if (this.taskService.currentTask.currentSession.status === TaskStatus.STARTED) {
            this.blockSpecification = this.taskService.currentTask.currentSession.currentBlock.trailSpecifications;
        }
        else if ((!this.taskService.currentTask.currentSession ||
            this.taskService.currentTask.currentSession.status === TaskStatus.COMPLETED.toString())
            && this.taskService.currentTask.transfer) {
            this.isTransfer = true;
            this.blockSpecification = this.taskService.currentTask.transfer.trailSpecifications;
        }

        this.player = new TNSPlayer();
        this.blockSpecification.forEach(specificationJson => {
            let response = new TrailResponse({
                response: ResponseType[ResponseType.TIMED_OUT],
                visualStimulusFilename: specificationJson.visualStimulusFilename
            });
            if (!this.isTransfer && specificationJson.auditoryStimulusFilename) {
                response.auditoryStimulusFilename = specificationJson.auditoryStimulusFilename;
            }
            this.responses.push(response);
        });
    }
    ngOnInit(): void {
        this.page.actionBarHidden = true;
        if (this.isTransfer) {
            this.onTransferTurn();
        }
        else {
            this.onTurn();
        }
    }

    private onTurn() {
        console.log("Start of turn: " + this.turnIndex);
        this.onHiddingImage();
        this.onButtonDisable();
        this.onPlayAudio(this.blockSpecification[this.turnIndex]);
    }

    private onTransferTurn() {
        console.log("Start of turn: " + this.turnIndex + " for Transfer");
        this.onPlayImage(this.blockSpecification[this.turnIndex]);
        this.id = setInterval(() => {
            console.log("No Selection made" + Date.now());
            clearInterval(this.id);
            dialogs.alert({
                title: "Alert",
                message: "Time Exceeded",
                okButtonText: "Ok"
            }).then(() => {
                this.onFinishTurn();
            });
            (error: any) => {
                console.log("Time exceeded");
                alert("Unfortunately unexpected error occured");
            }
        }, this.taskService.currentTask.transfer.timeoutNoticeDurationInSeconds * 1000)
        this.currentTurnInputStart = moment();
        this.onButtonReset();
    }

    private onHiddingImage() {
        this.showImage = false;
        let image: Image = this.img.nativeElement;
        image.visibility = "hidden";
    }

    private onPlayImage(specification: TrialSpecification) {
        this.showImage = true;
        let image: Image = this.img.nativeElement;
        image.visibility = "visible";
        image.src = this.visualPath + specification.visualStimulusFilename;
        image.colSpan = 2;
        image.horizontalAlignment = "center";
        image.stretch = "aspectFit";
    }
    private onPlayAudio(specification: TrialSpecification) {
        let filepath = this.audioPath + specification.auditoryStimulusFilename;
        try {
            const playerOptions: AudioPlayerOptions = {
                audioFile: filepath,
                loop: false,
                completeCallback: () => {
                    this.player.dispose().then(
                        () => {
                            // this.set("isPlaying", false);
                            console.log("DISPOSED");
                            this.onPlayImage(this.blockSpecification[this.turnIndex]);
                            this.id = setInterval(() => {
                                console.log("No Selection made" + Date.now());
                                clearInterval(this.id);
                                dialogs.alert({
                                    title: "Alert",
                                    message: "Time Exceeded",
                                    okButtonText: "Ok"
                                }).then(() => {
                                    this.onFinishTurn();
                                });
                                (error: any) => {
                                    console.log("Time exceeded");
                                    alert("Unfortunately unexpected error occured");
                                }
                            }, this.taskService.currentTask.currentSession.currentBlock.timeoutNoticeDurationInSeconds * 1000)
                            this.currentTurnInputStart = moment();
                            this.onButtonReset();
                        },
                        err => {
                            console.log("ERROR disposePlayer: " + err);
                        }
                    );
                },

                errorCallback: errorObject => {
                    console.log(JSON.stringify(errorObject));
                    // this.set("isPlaying", false);
                },

                infoCallback: args => {
                    console.log(JSON.stringify(args));

                    dialogs.alert("Info callback: " + args.info);
                    console.log(JSON.stringify(args));
                }
            };

            // this.set("isPlaying", true);

            this.player.playFromFile(playerOptions).then(
                () => {
                    console.log("Audio played " + playerOptions.audioFile);
                    // this.set("isPlaying", true);

                },
                err => {
                    console.log(err);
                    // this.set("isPlaying", false);
                }
            );
        } catch (ex) {
            console.log(ex);
        }
    }

    private onPlayBeepAudio() {
        let filepath = this.audioBeepPath + "answered.mp3";
        try {
            const playerOptions: AudioPlayerOptions = {
                audioFile: filepath,
                loop: false,
                completeCallback: () => {
                    this.player.dispose().then(
                        () => {
                            this.onFinishTurn();
                        },
                        err => {
                            console.log("ERROR disposePlayer: " + err);
                        }
                    );
                },

                errorCallback: errorObject => {
                    console.log(JSON.stringify(errorObject));
                },

                infoCallback: args => {
                    console.log(JSON.stringify(args));

                    dialogs.alert("Info callback: " + args.info);
                    console.log(JSON.stringify(args));
                }
            };

            this.player.playFromFile(playerOptions).then(
                () => {
                    console.log("Audio played " + playerOptions.audioFile);
                },
                err => {
                    console.log(err);
                }
            );
        } catch (ex) {
            console.log(ex);
        }
    }

    onButtonReset(): void {
        let yesButton: Button = this.btnYes.nativeElement;
        yesButton.isEnabled = true;
        yesButton.style.backgroundColor = new Color("#01a7f6");

        let noButton: Button = this.btnNo.nativeElement;
        noButton.isEnabled = true;

    }

    onButtonDisable(): void {
        let yesButton: Button = this.btnYes.nativeElement;
        yesButton.isEnabled = false;
        yesButton.style.backgroundColor = new Color("Gray");

        let noButton: Button = this.btnNo.nativeElement;
        noButton.isEnabled = false;

    }

    onYesTap(): void {
        this.onTap();
        this.responses[this.turnIndex].response = this.isTransfer ? TransferResponseType[TransferResponseType.FAMILIAR] : ResponseType[ResponseType.MATCH];
        console.log("turn: " + this.turnIndex) + " clicked Yes";
        // this.onFinishTurn();
    }

    onNoTap(): void {
        this.onTap();
        this.responses[this.turnIndex].response = this.isTransfer ? TransferResponseType[TransferResponseType.NOVEL] : ResponseType[ResponseType.NON_MATCH];
        console.log("turn: " + this.turnIndex) + " clicked No";
        // this.onFinishTurn();
    }

    onTap(): void {
        clearInterval(this.id);
        this.onButtonDisable();
        let endTime: moment.Moment = moment();
        let duration = moment.duration(endTime.diff(this.currentTurnInputStart)).asSeconds();
        console.log("Response time is " + duration + " seconds");
        this.responses[this.turnIndex].responseTimeInSeconds = duration;
        this.onPlayBeepAudio();
    }

    onFinishPopDialog(): void {
        dialogs.alert({
            title: "Congratulation",
            message: "Successful finish one trial",
            okButtonText: "Ok"
        }).then(() => {
            this.router.navigate(["/home"], { clearHistory: true });
        });
    }

    onFinishTurn(): void {
        console.log("Turn " + this.turnIndex + " had finished");
        this.turnIndex++;
        if (this.turnIndex >= this.responses.length) {
            let postTrailsHref = this.isTransfer ? this.taskService.currentTask.transferHref + "/completeTransfer" : this.taskService.currentTask.currentSession.currentBlock.blockHref + "/completeBlock";
            this.userService.postTrailsResponses(postTrailsHref, this.responses)
                .subscribe(finishedBlock => {
                    if (this.isTransfer) {
                        this.onFinishedUpdateTransfer(finishedBlock);
                    }
                    else {
                        this.onFinishedUpdateBlock(finishedBlock);
                    }
                })
            console.log(JSON.stringify(this.responses));
        }
        else {
            if (this.isTransfer) {
                this.onTransferTurn();
            }
            else {
                this.onTurn();
            }
        }
    }

    private onFinishedUpdateBlock(finishedBlock): void {
        this.taskService.currentTask.currentSession.currentBlock = this.taskService.createBlock(finishedBlock, false, this.responses);
        this.taskService.currentTask.currentSession.blocks.push(this.taskService.currentTask.currentSession.currentBlock);
        let tasks: Task[] = SettingService.history ? JSON.parse(SettingService.history) : [];
        let existingTaskIndex: number = tasks.findIndex(task => {
            return task.taskSpecification.code === this.taskService.currentTask.taskSpecification.code;
        })
        if (existingTaskIndex === -1) {
            tasks.push(this.taskService.currentTask);
        }
        else {
            tasks[existingTaskIndex] = this.taskService.currentTask;
        }
        SettingService.history = JSON.stringify(tasks);
        if (!this.taskService.currentTask.currentSessionHref) {
            console.log("No more block ending task");
            this.onFinishPopDialog();
            return;
        }
        if (!this.taskService.currentTask.currentSessionHref) {
            console.log("No more session");
            this.onFinishPopDialog();
            return;
        }
        this.userService.commonGetHttpCall(this.taskService.currentTask.currentSessionHref)
            .subscribe(session => {
                console.log("[Succssfull get details of a session: ]" + this.taskService.currentTask.sessionHref);
                this.taskService.setSession(session);
                if (!this.taskService.currentTask.currentSession.currentBlockHref) {
                    this.onFinishPopDialog();
                    return;
                }
                this.userService.commonGetHttpCall(this.taskService.currentTask.currentSession.currentBlockHref)
                    .subscribe(block => {
                        console.log("[Succssfull get details of a block: ]" + this.taskService.currentTask.currentSession.currentBlockHref);
                        this.taskService.setBlock(block);
                        if(this.taskService.currentTask.currentSession.currentBlock.status === TaskStatus[TaskStatus.NOT_STARTED]&&
                            this.taskService.currentTask.currentSession.currentBlock.earliestStartDateTime){
                            this.schedule(new Date(this.taskService.currentTask.currentSession.currentBlock.earliestStartDateTime));
                        }
                        this.onFinishPopDialog();
                    }),
                    (error: any) => {
                        console.log(error);
                    };
            }),
            (error: any) => {
                console.log(error);
            };
    }

    public schedule(readyDate: Date) {
        LocalNotifications.requestPermission().then(granted => {
            if(granted) {
                LocalNotifications.schedule([{
                    id: 1,
                    title: "Your next block is ready",
                    body: "Your next block is ready",
                    at: readyDate
                }]).then(() => {
                    console.log("Notification showed");
                }, error => {
                    console.dir(error);
                });
            }
        });
    }


    private onFinishedUpdateTransfer(finishedTransfer): void {
        this.taskService.currentTask.transfer = this.taskService.createBlock(finishedTransfer, true, this.responses);
        let tasks: Task[] = SettingService.history ? JSON.parse(SettingService.history) : [];
        let existingTaskIndex: number = tasks.findIndex(task => {
            return task.taskSpecification.code === this.taskService.currentTask.taskSpecification.code;
        })
        if (existingTaskIndex === -1) {
            tasks.push(this.taskService.currentTask);
        }
        else {
            tasks[existingTaskIndex] = this.taskService.currentTask;
        }
        SettingService.history = JSON.stringify(tasks);
        if (!this.taskService.currentTask.currentSessionHref) {
            console.log("No more block ending task");
            this.onFinishPopDialog();
            return;
        }
        if (!this.taskService.currentTask.currentSessionHref) {
            console.log("No more session");
            this.onFinishPopDialog();
            return;
        }
        this.userService.commonGetHttpCall(this.taskService.currentTask.currentSessionHref)
            .subscribe(session => {
                console.log("[Succssfull get details of a session: ]" + this.taskService.currentTask.sessionHref);
                this.taskService.setSession(session);
                if (!this.taskService.currentTask.currentSession.currentBlockHref || this.isTransfer) {
                    this.onFinishPopDialog();
                    return;
                }
                this.userService.commonGetHttpCall(this.taskService.currentTask.currentSession.currentBlockHref)
                    .subscribe(block => {
                        console.log("[Succssfull get details of a block: ]" + this.taskService.currentTask.currentSession.currentBlockHref);
                        this.taskService.setBlock(block);
                        this.onFinishPopDialog();
                    }),
                    (error: any) => {
                        console.log(error);
                    };
            }),
            (error: any) => {
                console.log(error);
            };
    }

}

