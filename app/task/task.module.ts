import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { TaskRoutingModule } from "./task-routing.module";
import { TaskComponent } from "./task.component";

@NgModule({
    imports: [
        NativeScriptModule,
        TaskRoutingModule,
        NativeScriptFormsModule
    ],
    declarations: [
        TaskComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class TaskModule { }